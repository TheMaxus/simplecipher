import base64
import binascii
import codecs


class Encoding:
    def base64Encode(self, text):
        return bytes.decode(base64.standard_b64encode(bytes(text, "UTF-8")))

    def base64Decode(self, text):
        try:
            output = bytes.decode(
                base64.standard_b64decode(bytes(text, "ASCII")))
        except binascii.Error:
            return "Incorrect padding!"
        except UnicodeEncodeError:
            return "Error!"
        except UnicodeDecodeError:
            return "Error!"
        return output

    def binToText(self, text):
        text = text.replace(' ', '')
        chars = []
        byte = ''
        for y, x in enumerate(text):
            if x not in '10':
                return None
            if y % 8 < 7:
                byte = ''.join([byte, x])
            else:
                byte = ''.join([byte, x])
                byteAscii = chr(int(byte, 2))
                chars.append(byteAscii)
                byte = ''
        return ''.join(chars)

    def textToBin(self, text):
        outputText = ''
        for x in text:
            char = "{0:b}".format(ord(x))
            outputText = ''.join([outputText, char.zfill(8)])
        return outputText

    def autoDecode(self, text):
        outputText = text
        notBinary = True
        notBase64 = True
        while notBinary or notBase64:
            if self.binToText(outputText) == None:
                notBinary = False
            else:
                outputText = self.binToText(outputText)
                notBinary = True

            if self.base64Decode(outputText) == "Error!" or self.base64Decode(outputText) == "Incorrect padding!":
                notBase64 = False
            else:
                outputText = self.base64Decode(outputText)
                notBase64 = True
        return outputText
