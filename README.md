# SimpleCipher

SimpleCipher is a program written in Python 3 which can be used to encrypt and decrypt text using simple mostly obsolete
cipher algorithms. It can also decode ASCII binary and Base64, perform frequency analysis and bruteforce some ciphers by outputing decrypted message for each possible key.

## Currently implemented ciphers:

 - ROT13
 - Atbash
 - Simple Substitution
 - Vigenere
    - Varant Beaufort
    - Beaufort
 - Rail Fence
