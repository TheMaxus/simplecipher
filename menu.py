from analysis import Analysis
from ciphers.caesar import Caesar
from ciphers.substitution import Substitution
from ciphers.transposition import Transposition
from ciphers.vigenere import Vigenere
from config import Config
from encoding import Encoding
from tools import Tools


class Menu:

    conf = Config
    encDecSelect = '\n 1: Encrypt\n 2: Decrypt \n\n>>> '

    def menuMain(self):
        while True:
            t = Tools()
            self.conf.printSettings(self.conf)
            menuText = '''
 1: Input message
 2: Input key
 3: Ciphers
 4: Analysis
 5: Encoding
 help: view other commands
                 
 >>> '''

            menu = input(menuText)
            if menu == '1':
                self.conf.message = t.inputMessage()
                self.alphabetCheck()
                self.caseDetection()
            elif menu == '2':
                self.conf.key = input("Input key >>> ")
            elif menu == '3':
                self.menuCiphers()
            elif menu == '4':
                self.menuAnalysis()
            elif menu == '5':
                self.menuEncodings()
            elif menu == 'strip':
                self.conf.message = t.stripFormatting(
                    self.conf.message, self.conf.alphabet, self.conf.distanceBetweenSpaces)
            elif menu == 'upper':
                self.conf.upper(self.conf)
            elif menu == 'lower':
                self.conf.lower(self.conf)
            elif menu == 'mc':
                self.conf.message, self.conf.ciphertext = self.conf.ciphertext, self.conf.message
            elif menu == 'mk':
                self.conf.message, self.conf.key = self.conf.key, self.conf.message
            elif menu == 'ck':
                self.conf.ciphertext, self.conf.key = self.conf.key, self.conf.ciphertext
            elif menu == 'bin':
                self.menuEncodings('binary')
            elif menu == 'b64':
                self.menuEncodings('base64')
            elif menu == 'a':
                self.menuEncodings('auto')
            elif menu == 'help':
                self.help()
            elif menu == 'case':
                self.caseDetection()
            elif menu == 'abc':
                self.alphabetCheck()
            elif menu == 'quit' or menu == 'exit' or menu == 'q':
                exit()

    def menuCiphers(self):
        self.conf.printSettings(self.conf)
        menu = input(
            "\n 1: Caesar cipher\n 2: Substitution cipher\n 3: Vigenere and variants\n 4: Transposition\n 0: Return\n\n>>> ")
        if menu == '1':
            try:
                self.menuCaesar()
            except ValueError:
                print("\nKey needs to be a number!")
        elif menu == '2':
            if self.conf.key == '':
                print("\nInput key first!")
                return
            self.menuSubstitution()
        elif menu == '3':
            try:
                self.menuVigenere()
            except ZeroDivisionError:
                print("\nInput key first!")
        elif menu == '4':
            try:
                self.menuTransposotion()
            except ValueError:
                print("\nKey needs to be a number!")
        elif menu == '0':
            return

    def menuCaesar(self):
        c = Caesar()
        self.conf.printSettings(self.conf)
        menu = input(
            "\n 1: Encrypt (Negative key value to decrypt)\n 2: Bruteforce\n 3: ROT13\n0: Return\n\n>>> ")
        if menu == '1':
            self.getOutput(c.caesar(int(self.conf.key),
                                    self.conf.message, self.conf.alphabet))
        elif menu == '2':
            self.processCaesarBruteforce(
                c.brute(self.conf.message, self.conf.alphabet))
        elif menu == '3':
            menu = input(self.encDecSelect)
            if menu == 1:
                self.getOutput(
                    c.caesar(13, self.conf.message, self.conf.alphabet))
            else:
                self.getOutput(
                    c.caesar(-13, self.conf.message, self.conf.alphabet))
        elif menu == '0':
            return

    def menuSubstitution(self):
        s = Substitution()
        self.conf.printSettings(self.conf)
        menu = input(
            "\n 1: Simple substitution\n 2: Atbash\n 0: Return\n\n>>> ")
        if menu == '1':
            menu = input(self.encDecSelect)
            if menu == '1':
                self.getOutput(s.substitution(
                    self.conf.key, self.conf.message, self.conf.alphabet, 0))
            else:
                self.getOutput(s.substitution(
                    self.conf.key, self.conf.message, self.conf.alphabet, 1))
        elif menu == '2':
            self.getOutput(s.atbash(self.conf.message, self.conf.alphabet))
        elif menu == '0':
            return

    def menuVigenere(self):
        v = Vigenere()
        self.conf.printSettings(self.conf)
        menu = input(
            "\n 1: Vigenere\n 2: Variant Beaufort\n 3: Beaufort\n 0: Return\n\n>>> ")
        if menu == '1':
            menu = input(self.encDecSelect)
            if menu == '1':
                self.getOutput(v.vigenere(self.conf.message,
                                          self.conf.key, self.conf.alphabet))
            else:
                self.getOutput(v.vigenere(self.conf.message,
                                          self.conf.key, self.conf.alphabet, -1))
        elif menu == '2':
            menu = input(self.encDecSelect)
            if menu == '1':
                self.getOutput(v.variantBeaufort(self.conf.message,
                                                 self.conf.key, self.conf.alphabet))
            else:
                self.getOutput(v.variantBeaufort(self.conf.message,
                                                 self.conf.key, self.conf.alphabet, -1))
        elif menu == '3':
            self.getOutput(v.beaufort(self.conf.message,
                                      self.conf.key, self.conf.alphabet))
        elif menu == '0':
            return

    def menuTransposotion(self):
        t = Transposition()
        self.conf.printSettings(self.conf)
        menu = input(
            "\n 1: Rail fence\n 0: Return\n\n>>> ")

        if menu == '1':
            menu = input("\n 1: Encrypt\n 2: Decrypt\n 3: Bruteforce \n\n>>>")
            if menu == '1':
                self.getOutput(t.railFenceEncrypt(self.conf.message,
                                                  int(self.conf.key)))
            elif menu == '2':
                self.getOutput(t.railFenceDecrypt(self.conf.message,
                                                  int(self.conf.key)))
            elif menu == '3':
                self.getOutput(t.bruteforce(self.conf.message))

    def menuAnalysis(self):
        menu = input(" 1: Frequency analysis\n 0: Return\n\n>>>")
        if menu == '1':
            a = Analysis()
            print(self.processFreqAnalyzis(a.freqAnalysis(self.conf.message)))

    def menuEncodings(self, menu=''):
        e = Encoding()
        if menu == 'binary':
            self.getOutput(e.binToText(self.conf.message))
        elif menu == 'base64':
            self.getOutput(e.base64Decode(self.conf.message))
        elif menu == 'auto':
            self.getOutput(e.autoDecode(self.conf.message))
        else:
            menu = input(" 1: Base64\n 2: Binary\n 0: Return \n\n>>> ")
            if menu == '1':
                menu = input(" 1: Base64 to text\n 2: Text to base64 \n\n>>>")
                if menu == '1':
                    self.getOutput(e.base64Decode(self.conf.message))
                elif menu == '2':
                    self.getOutput(e.base64Encode(self.conf.message))
                else:
                    return
            elif menu == '2':
                menu = input(" 1: Binary to text\n 2: Text to binary \n\n>>>")
                if menu == '1':
                    self.getOutput(e.binToText(self.conf.message))
                elif menu == '2':
                    self.getOutput(e.textToBin(self.conf.message))
                else:
                    return
            elif menu == '0':
                return

    def help(self):
        helpText = '''
mc - set output of the previous command as message and vice versa
mk - set key as message and vice versa
ck - ciphertext to key and vice versa
strip - strips formatting of the string (removes everything except for letters from the alphabet)
exit or quit or just q - exit the program'''
        
        print(helpText)
        input()

    def getOutput(self, output):
        if output == '':
            return
        print("\n" + output)
        self.conf.ciphertext = output

    def processFreqAnalyzis(self, inputArray):
        length = 0
        statList = []
        for x, __ in inputArray:
            length += x
        statList.append("Total characters: {0}\n ".format(length))

        for x, y in inputArray:
            occurancePercent = round(x/length*100, 2)
            letterStats = "{0} : {1} : {2}% | ".format(y, x, occurancePercent)
            statList.append(letterStats)
            if statList.index(letterStats) % 9 == 8:
                statList.append("\n")
        return ''.join(statList)

    def processCaesarBruteforce(self, inputArray):
        for x, y in enumerate(inputArray):
            print("{0}: {1}".format(x, y))

    def caseDetection(self):
        lowercase = 0
        uppercase = 0
        undefined = 0

        for x in self.conf.message:
            if x in self.conf.alphabet.lower():
                lowercase += 1
            elif x in self.conf.alphabet.upper():
                uppercase += 1
            elif x != ' ':
                undefined += 1

        if lowercase > uppercase and lowercase > undefined:
            self.conf.message = self.conf.message.lower()
            self.conf.alphabet = self.conf.alphabet.lower()
        elif uppercase > lowercase and uppercase > undefined:
            self.conf.message = self.conf.message.upper()
            self.conf.alphabet = self.conf.alphabet.upper()
        else:
            self.alphabetCheck()

    def alphabetCheck (self):
        alphabetMatches = []
        for currentAlphabet in self.conf.alphabets:
            alphabetMatches.append(0)
            for x in currentAlphabet:
                alphabetMatches[-1] += self.conf.message.count(x)
        
        
        
        self.conf.alphabet = self.conf.alphabets[alphabetMatches.index(max(alphabetMatches))]
