class Analysis:
    def freqAnalysis(self, text):
        text = text.replace(' ', '')
        result = []
        chars = []
        for x in text:
            if x not in chars:
                chars.append(x)
        for x in chars:
            result.append([text.count(x), x])
            text = text.replace(x, '')
        result.sort(reverse=True)
        return result
