# Various tools for formatting the text


class Tools:
    def inputMessage(self):
        message = input("Type message you want to encrypt or decrypt >>> ")
        return message

    def stripFormatting(self, string, alphabet, wordLengh=5):
        outputString = ''
        chars = 0
        for x in string:
            if chars == wordLengh:
                outputString += ' '
                chars = 0
            if x not in alphabet:
                pass
            else:
                outputString += x
                chars += 1
        if outputString == '':
            return string
        else:
            return outputString
