from analysis import Analysis


def test_freqAnalysis():
    a = Analysis()
    assert a.freqAnalysis("abbccc") == [[3, 'c'], [2, 'b'], [1, 'a']]
