import string

from ciphers.vigenere import Vigenere

alphabet = string.ascii_uppercase
v = Vigenere()


def test_vignere_encypher():
    assert v.vigenere("ATTACKATDAWN", "LEMON", alphabet) == "LXFOPVEFRNHR"


def test_spaces():
    assert v.vigenere("ATTACK AT DAWN", "LEMON", alphabet) == "LXFOPV EF RNHR"


def test_vignere_decypher():
    assert v.vigenere("LXFOPVEFRNHR", "LEMON", alphabet, -1) == "ATTACKATDAWN"


def test_variant_beaufort():
    assert v.variantBeaufort("HELLO", "LOST", alphabet) == "WQTSD"


def test_beaufort():
    "BEAUFORT" == v.beaufort(v.beaufort(
        "BEAUFORT", "KEY", alphabet), "KEY", alphabet)
    assert v.beaufort("BEAUFORT", "KEY", alphabet) != "BEAUFORT"
