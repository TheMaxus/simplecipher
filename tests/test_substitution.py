import string

from ciphers.substitution import Substitution

s = Substitution()
a = string.ascii_uppercase


def test_atbash():
    assert s.atbash("AZ", a) == "ZA"
    assert s.atbash("ABCDEFG", a) == "ZYXWVUT"


def test_spaces():
    assert s.atbash("ABCD EFG", a) == "ZYXW VUT"


def test_simpeSubstitution():
    assert s.substitution("ZEBRASCDFGHIJKLMNOPQTUVWXY", "flee at once. we are discovered!".upper(
    ), a) == "SIAA ZQ LKBA. VA ZOA RFPBLUAOAR!"
