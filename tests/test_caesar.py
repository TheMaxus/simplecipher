from ciphers.caesar import Caesar
import string
alphabet = string.ascii_uppercase

c = Caesar()


def test_Caesar():
    assert c.caesar(13, "Why did the chicken cross the road?".upper(
    ), alphabet) == "Jul qvq gur puvpxra pebff gur ebnq?".upper()
