from encoding import Encoding

e = Encoding()


def test_base64Enc():
    assert e.base64Encode("base64") == "YmFzZTY0"


def test_base64Dec():
    assert e.base64Decode("YmFzZTY0") == "base64"


def test_base64DecExceptionHandling():
    assert e.base64Decode("YmFzZTY0aaa") == "Incorrect padding!"
    assert e.base64Decode("YmFzZTY0ö") == "Error!"


def test_binToAscii():
    assert e.binToText(
        '011100000111100101110100011010000110111101101110') == "python"
    assert e.binToText("asd") == None


def test_TextToBin():
    assert e.textToBin(
        "python") == "011100000111100101110100011010000110111101101110"
