from ciphers.transposition import Transposition

t = Transposition()


def test_railfenceEncrypt():
    assert t.railFenceEncrypt(
        "WEAREDISCOVEREDFLEEATONCE", 3) == "WECRLTEERDSOEEFEAOCAIVDEN"


def test_railfenceDecrypt():
    assert t.railFenceDecrypt(
        "WECRLTEERDSOEEFEAOCAIVDEN", 3) == "WEAREDISCOVEREDFLEEATONCE"


def test_bruteforce():
    assert "WEAREDISCOVEREDFLEEATONCE" in t.bruteforce(
        "WECRLTEERDSOEEFEAOCAIVDEN")
