from ciphers.substitution import Substitution

# Viegnere cipher and direvatives like Beaufort


class Vigenere:
    def vigenere(self, inputText, key, alphabet, decrypt=1, alphabet2=''):
        outputText = ''
        if alphabet2 == '':
            alphabet2 = alphabet
        spaces = 0
        for x in range(len(inputText)):
            if inputText[x] == ' ':
                outputText = ''.join([outputText, inputText[x]])
                spaces += 1
            else:
                rotation = (alphabet.index(
                    key[(x-spaces) % len(key)]) * decrypt + alphabet.index(inputText[x])) % len(alphabet)
                outputText = ''.join(
                    [outputText, alphabet2[rotation]])
        return outputText

    def variantBeaufort(self, inputText, key, alphabet, decrypt=1):
        return self.vigenere(inputText, key, alphabet, decrypt*-1)

    def beaufort(self, inputText, key, alphabet):
        s = Substitution()
        alphabet2 = s.atbash(alphabet, alphabet)
        return self.vigenere(inputText, key, alphabet, 1, alphabet2)
