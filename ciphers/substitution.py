# Simple substitution cipher and other variants like atbash


class Substitution:
    def atbash(self, inputString, alphabet):
        key = alphabet[::-1]
        return self.substitution(key, inputString, alphabet)

    def substitution(self, key, inputString, alphabet, encDec=0):  # 0: Encrypt 1: Decrypt
        outputString = ''
        if encDec:
            alphabet, key = key, alphabet

        for x in inputString:
            if x not in alphabet:
                outputString = ''.join([outputString, x])
            elif len(key) == len(alphabet):
                outputString = ''.join(
                    [outputString, key[alphabet.index(x) % len(alphabet)]])
            else:
                print("Key and alphabet must be the same size!")
                break
        return outputString
