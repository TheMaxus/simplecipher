import itertools


class Transposition:
    def railFenceEncrypt(self, text, height):
        rows = [''] * height
        rowIterator = itertools.chain(
            range(0, height-1), range(height-1, 0, -1))
        for x, y in zip(text, itertools.cycle(rowIterator)):
            rows[y] = ''.join([rows[y], x])
        return ''.join(rows)

    def railFenceDecrypt(self, text, height):
        outputText = ''
        rows = [''] * height
        rowLength = [0] * height
        inputLength = len(text)

        if height > inputLength:
            raise Exception


        # Custom iterator which goes to hight and back to zero, creating a triangle wave needed for railfence cipher
        rowIterator = itertools.chain(
            range(0, height-1), range(height-1, 0, -1))

        cycles = inputLength//((height*2)-2)
        rowLength[0] = cycles
        rowLength[height-1] = cycles

        for x in range(1, height-1):
            rowLength[x] = cycles*2

        if cycles != 0:
            for x in (range(inputLength % cycles)):
                rowLength[x] += 1

        previousPosition = 0
        for x, y in zip(rowLength, range(height)):
            for z in range(previousPosition, previousPosition + x):
                rows[y] = ''.join([rows[y], text[z]])
            previousPosition += x

        rowPosition = [0] * height
        for __, y in zip(range(inputLength), itertools.cycle(rowIterator)):
            if rowPosition < rowLength:
                outputText = ''.join([outputText, rows[y][rowPosition[y]]])
                rowPosition[y] += 1
        return outputText

    def bruteforce(self, text):
        possibleStrings = ''
        for x in range(2, len(text)):
            if self.railFenceDecrypt(text, x) != '':
                possibleStrings = possibleStrings + str(x) + self.railFenceDecrypt(text, x) + '\n'

        return str(possibleStrings)
