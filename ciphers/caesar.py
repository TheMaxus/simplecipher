# Caesar cipher and other variants like ROT13


class Caesar:
    def caesar(self, rotation, inputString, alphabet):
        outputString = ''

        for x in inputString:
            if x not in alphabet:
                outputString = ''.join([outputString, x])
            else:
                outputString = ''.join(
                    [outputString, alphabet[(alphabet.index(x) + rotation) % len(alphabet)]])

        return outputString

    def brute(self, message, alphabet):
        possibleStrings = []
        for y in range(len(alphabet)):
            possibleStrings.append(self.caesar(-y, message, alphabet))
        return possibleStrings
