#!/usr/bin/env python3

from ciphers.caesar import Caesar
from ciphers.substitution import Substitution
from tools import Tools
from menu import Menu


def main():

    m = Menu()
    m.menuMain()


if __name__ == '__main__':
    main()
