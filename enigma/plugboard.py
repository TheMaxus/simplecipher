import string


class Plugboard:

    def __init__(self, plugobard=string.ascii_uppercase):
        self.wiring = {}
        for x in string.ascii_uppercase:
            self.wiring[x] = x

    def transform(self, letter):
        letter = self.wiring[letter]
        return letter
