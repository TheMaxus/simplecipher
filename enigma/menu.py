import itertools
import string

from plugboard import Plugboard
from rotor import Rotor

plugboard = Plugboard()
ETW = Rotor('0', 'QWERTZUIOASDFGHJKPYXCVBNML')
III = Rotor('0', 'JVIUBHTCDYAKEQZPOSGXNRMWFL', False, 'Q')
I = Rotor('0', 'JGDQOXUSCAMIFRVTPNEWKBLZYH')
II = Rotor('0', 'NTZPSFBOKMWRCJDIVLAEYUXHGQ')
IV = Rotor('0')
UKW = Rotor('0', 'QYHOGNECVPUZTFDJAXWMKISRBL')

order = [plugboard, ETW, III, I, II, IV]


a = ''

def encrypt(message):
    output = []
    for a in message:
        III.turn()
        for x in order:
            a = x.transform(a)

        a = UKW.transform(a)
        UKW.reverse = False

        for x in order[::-1]:
            a = x.transform(a)
        output.append(a)
    return ''.join(output)

print(encrypt("ASD"))
print(encrypt(encrypt("ASD")))
