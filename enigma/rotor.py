import string


class Rotor(object):

    def __init__(self, position=0, wiring=string.ascii_uppercase, stationary=True, turnover='1'):

        self.position = 0
        self.wiring = {}
        self.wiringReverse = {}
        self.notch = 0
        self.turnover = 0
        self.alphabet = string.ascii_uppercase
        self.stationary = False
        self.reverse = False

        self.stationary = stationary

        if position in string.digits:
            self.position = (int(position)) % 26
        if position in string.ascii_uppercase:
            self.position = string.ascii_uppercase.index(position)

        if turnover in string.digits:
            self.turnover = (int(turnover)) % 26
        if turnover in string.ascii_uppercase:
            self.turnover = string.ascii_uppercase.index(turnover)

        for y, x in zip(wiring, string.ascii_uppercase):
            self.wiring[x] = y

        for y, x in zip(wiring, string.ascii_uppercase):
            self.wiringReverse[y] = x

    def transform(self, letter):
        if self.reverse:
            reverseMultiplier = -1
        else:
            reverseMultiplier = 1
        letterNumber = 0
        letterNumber = self.alphabet.index(letter)
        letterNumber = (letterNumber + self.position * reverseMultiplier) % 26

        letter = self.alphabet[letterNumber]
        if not self.reverse:
            letter = self.wiring[letter]
            self.reverse = True
        else:
            letter = self.wiringReverse[letter]
            self.reverse = False
        return letter

    def turn(self):
        if not self.stationary:
            self.position = (self.position + 1) % 26
