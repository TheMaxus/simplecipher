import string


class Config:

    enableStripFormatting = 0
    distanceBetweenSpaces = 5
    alphabets = [string.ascii_lowercase]
    alphabet = string.ascii_lowercase
    key = ''
    message = ''
    ciphertext = ''

    def upper(self):
        self.alphabet = self.alphabet.upper()
        self.key = self.key.upper()
        self.message = self.message.upper()

    def lower(self):
        self.alphabet = self.alphabet.lower()
        self.key = self.key.lower()
        self.message = self.message.lower()

    def printSettings(self):
        if self.message != '':
            print("Message:", self.message)
        if self.ciphertext != '':
            print("Ciphertext:", self.ciphertext)
        if self.key != '':
            print("Key:", self.key)
        if self.alphabet != '':
            print("Current alphabet:", self.alphabet)
